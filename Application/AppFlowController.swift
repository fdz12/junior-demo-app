import Resolver
import SharedDomain
import UIKit
import UIToolkit

final class AppFlowController: FlowController, MainFlowControllerDelegate {
    func presentOnboarding(animated: Bool, completion: (() -> Void)?) {
        let fc = MainFlowController(navigationController: navigationController)
        fc.delegate = self
        let rootVC = startChildFlow(fc)
        navigationController.viewControllers = [rootVC]
    }
    
    @Injected private var handlePushNotificationUseCase: HandlePushNotificationUseCase
    
    func start() {
        setupMain()
    }
    
    func setupMain() {
        let fc = MainFlowController(navigationController: navigationController)
        fc.delegate = self
        let rootVC = startChildFlow(fc)
        navigationController.viewControllers = [rootVC]
    }
    
    public func handlePushNotification(_ notification: [AnyHashable: Any]) {
        guard let main = childControllers.first(where: { $0 is MainFlowController }) as? MainFlowController else { return }
        do {
            let notification = try handlePushNotificationUseCase.execute(notification)
            main.handleDeeplink(for: notification)
        } catch {}
    }
}
