import AnalyticsToolkit
import LocationToolkit
import PushNotificationsToolkit
import RemoteConfigToolkit
import Resolver
import SharedDomain
import TransactionsToolkit

public extension Resolver {
    static func registerRepositories() {
        register { AnalyticsRepositoryImpl(analyticsProvider: resolve()) as AnalyticsRepository }
        
        register { LocationRepositoryImpl(locationProvider: resolve()) as LocationRepository }
        
        register { PushNotificationsRepositoryImpl(pushNotificationsProvider: resolve()) as PushNotificationsRepository }
        
        register { RemoteConfigRepositoryImpl(remoteConfigProvider: resolve()) as RemoteConfigRepository }
        
        register {
            TransactionsRepositoryImpl(
                networkProvider: resolve()
            ) as TransactionsRepository
        }
    }
}
