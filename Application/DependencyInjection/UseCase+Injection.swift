import Resolver
import SharedDomain

public extension Resolver {
    static func registerUseCases() {
        // Analytics
        register { TrackAnalyticsEventUseCaseImpl(analyticsRepository: resolve()) as TrackAnalyticsEventUseCase }
        
        // Location
        register { GetCurrentLocationUseCaseImpl(locationRepository: resolve()) as GetCurrentLocationUseCase }
        
        // PushNotification
        register { HandlePushNotificationUseCaseImpl(pushNotificationsRepository: resolve()) as HandlePushNotificationUseCase }
        register { RegisterForPushNotificationsUseCaseImpl(pushNotificationsRepository: resolve()) as RegisterForPushNotificationsUseCase }
        
        // RemoteConfig
        register { GetRemoteConfigValueUseCaseImpl(remoteConfigRepository: resolve()) as GetRemoteConfigValueUseCase }
        
        // Transactions
        register { GetTransactionsUseCaseImpl(transactionsRepository: resolve()) as GetTransactionsUseCase }
    }
}
