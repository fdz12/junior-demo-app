import SharedDomain
import Transactions
import UIKit
import UIToolkit

enum MainTab: Int {
    case transactions = 1
}

protocol MainFlowControllerDelegate: AnyObject {
    func presentOnboarding(animated: Bool, completion: (() -> Void)?)
}

final class MainFlowController: FlowController {
    
    weak var delegate: MainFlowControllerDelegate?
    
    override func setup() -> UIViewController {
        let main = UITabBarController()
        main.viewControllers = [setupTransactionsTab()]
        return main
    }
    
    private func setupTransactionsTab() -> UINavigationController {
        let transactionsNC = BaseNavigationController()
        transactionsNC.tabBarItem = UITabBarItem(
            title: "Transactions",
            image: UIImage(systemName: "creditcard.fill"),
            tag: MainTab.transactions.rawValue
        )
        let transactionsFC = TransactionsFlowController(navigationController: transactionsNC)
        let transactionsRootVC = startChildFlow(transactionsFC)
        transactionsNC.viewControllers = [transactionsRootVC]
        return transactionsNC
    }
    
    @discardableResult private func switchTab(_ index: MainTab) -> FlowController? {
        guard let tabController = rootViewController as? UITabBarController,
            let tabs = tabController.viewControllers, index.rawValue < tabs.count else { return nil }
        tabController.selectedIndex = index.rawValue
        return childControllers[index.rawValue]
    }
    
    func handleDeeplink(for notification: PushNotification) {
        switch notification.type {
        case .userDetail:
            handleUserDetailDeeplink(userId: notification.entityId)
        default:
            return
        }
    }
    
    private func handleUserDetailDeeplink(userId: String) {
//        guard let usersFlowController = switchTab(.users) as? UsersFlowController else { return }
//        usersFlowController.handleUserDetailDeeplink(userId: userId)
    }
}
