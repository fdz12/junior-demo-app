// sourcery: AutoMockable
public protocol AnalyticsProvider {
    /// Track a given event
    func track(_ name: String, params: [String: AnyHashable])
}
