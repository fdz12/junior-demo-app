public enum DatabaseProviderError: Error {
    case typeNotRepresentable
    case objectNotFound
}
