import RealmSwift

public enum UpdateModel {
    case apiModel
    case fullModel
    
    func value(for object: Object) -> Any {
        switch self {
        case .apiModel: return object.apiModel
        case .fullModel: return object.fullModel
        }
    }
}
