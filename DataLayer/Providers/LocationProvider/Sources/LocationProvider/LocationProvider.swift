import CoreLocation

// sourcery: AutoMockable
public protocol LocationProvider {
    
    /// Check whether the location services are enabled and authorized
    func isLocationEnabled() -> Bool
    
    /// Observe current location
    func getCurrentLocation(withAccuracy accuracy: CLLocationAccuracy) -> AsyncStream<CLLocation>
}
