// sourcery: AutoMockable
public protocol RemoteConfigProvider {
    /// Try to read a value for the given key
    func read(_ key: String) async throws -> Bool
}
