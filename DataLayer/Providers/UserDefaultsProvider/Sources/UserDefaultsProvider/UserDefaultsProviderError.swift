public enum UserDefaultsProviderError: Error {
    case invalidBundleIdentifier
    case valueForKeyNotFound
}
