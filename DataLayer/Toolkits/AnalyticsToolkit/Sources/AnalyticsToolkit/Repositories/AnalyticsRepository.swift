import AnalyticsProvider
import SharedDomain

public struct AnalyticsRepositoryImpl: AnalyticsRepository {
    
    private let analytics: AnalyticsProvider
    
    public init(analyticsProvider: AnalyticsProvider) {
        analytics = analyticsProvider
    }
    
    public func create(_ event: AnalyticsEvent) {
        analytics.track(event.name, params: event.params)
    }
}
