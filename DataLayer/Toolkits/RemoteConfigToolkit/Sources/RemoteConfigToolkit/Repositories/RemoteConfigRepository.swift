import RemoteConfigProvider
import SharedDomain

public struct RemoteConfigRepositoryImpl: RemoteConfigRepository {
    
    private let remoteConfig: RemoteConfigProvider
    
    public init(remoteConfigProvider: RemoteConfigProvider) {
        remoteConfig = remoteConfigProvider
    }
    
    public func read(_ key: RemoteConfigCoding) async throws -> Bool {
        try await remoteConfig.read(key.rawValue)
    }
}
