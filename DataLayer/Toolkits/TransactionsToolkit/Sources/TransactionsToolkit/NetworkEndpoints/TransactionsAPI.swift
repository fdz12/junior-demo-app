import Foundation
import NetworkProvider
import Utilities

enum TransactionsAPI {
    case list
}

extension TransactionsAPI: NetworkEndpoint {
    var baseURL: URL { URL(string: "\((NetworkingConstants.baseTransactionsURL))/v1")! }
    
    var path: String {
        switch self {
        case .list:
            return "/customers/3"
        }
    }
    
    var method: NetworkMethod {
        return .get
    }
    var headers: [String: String]? {
        nil
    }
    var task: NetworkTask {
        switch self {
        case .list:
            let params: [String: Any] = [:]
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        }
    }
}
