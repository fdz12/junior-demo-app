import NetworkProvider
import OSLog
import SharedDomain

public struct TransactionsRepositoryImpl: TransactionsRepository {
    private let network: NetworkProvider
    
    public init(
        networkProvider: NetworkProvider
    ) {
        network = networkProvider
    }
    
    public func list() async throws -> [TransactionItem] {
        var transactionsArr: [TransactionItem] = []
        do {
            let transactions = try await network.request(TransactionsAPI.list)
            let json: [String: Any]?
            do {
                json = try JSONSerialization.jsonObject(with: transactions, options: []) as? [String: Any]
                let results = json?["transactions"] as? [[String: Any]] ?? [[String: Any]]()
                for result in results {
                    let dateStr: String = (result["date"] as? String ?? "")
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
                    let date = dateFormatter.date(from: dateStr)
                    
                    let transaction = TransactionItem(
                        id: result["id"] as? Int ?? 0,
                        type: result["type"] as? String ?? "",
                        description: result["description"] as? String ?? "",
                        date: date ?? Date(),
                        amount: result["amount"] as? Double ?? 0.0
                    )
                    transactionsArr.append(transaction)
                }
                return transactionsArr
            } catch {
                return transactionsArr
            }
        } catch {
            return transactionsArr
        }
    }
}
