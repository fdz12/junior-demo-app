@testable import TransactionsToolkit
import XCTest

final class TransactionsToolkitTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(TransactionsToolkit().text, "Hello, World!")
    }
}
