public protocol Trackable {
    var analyticsEvent: AnalyticsEvent { get }
}

public struct AnalyticsEvent: Equatable {
    public let name: String
    public let params: [String: AnyHashable]
    
    public init(
        name: String,
        params: [String: AnyHashable] = [:]
    ) {
        self.name = name
        self.params = params
    }
}
