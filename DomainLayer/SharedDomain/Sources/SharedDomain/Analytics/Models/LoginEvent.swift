public enum LoginEvent {
    case screenAppear
    case loginButtonTap
    case registerButtonTap
}

extension LoginEvent: Trackable {
    public var analyticsEvent: AnalyticsEvent {
        switch self {
        case .screenAppear: return .init(name: "login_screen")
        case .loginButtonTap: return .init(name: "login_button_tap")
        case .registerButtonTap: return .init(name: "register_button_tap")
        }
    }
}
