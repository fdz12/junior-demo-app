public enum UserEvent {
    case userDetail(id: String)
}

extension UserEvent: Trackable {
    public var analyticsEvent: AnalyticsEvent {
        switch self {
        case .userDetail(let id): return .init(name: "user_detail", params: ["id": id])
        }
    }
}
