// sourcery: AutoMockable
public protocol AnalyticsRepository {
    func create(_ event: AnalyticsEvent)
}
