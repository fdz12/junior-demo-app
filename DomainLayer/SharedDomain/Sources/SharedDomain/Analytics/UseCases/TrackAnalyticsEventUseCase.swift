// sourcery: AutoMockable
public protocol TrackAnalyticsEventUseCase {
    func execute(_ event: AnalyticsEvent)
}

public struct TrackAnalyticsEventUseCaseImpl: TrackAnalyticsEventUseCase {
    
    private let analyticsRepository: AnalyticsRepository
    
    public init(analyticsRepository: AnalyticsRepository) {
        self.analyticsRepository = analyticsRepository
    }
    
    public func execute(_ event: AnalyticsEvent) {
        analyticsRepository.create(event)
    }
}
