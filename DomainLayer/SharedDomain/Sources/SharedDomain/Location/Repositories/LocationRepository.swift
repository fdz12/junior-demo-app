import CoreLocation

// sourcery: AutoMockable
public protocol LocationRepository {
    func readIsLocationEnabled() -> Bool
    func readCurrentLocation(withAccuracy accuracy: CLLocationAccuracy) -> AsyncStream<CLLocation>
}
