import CoreLocation

// sourcery: AutoMockable
public protocol GetCurrentLocationUseCase {
    func execute() -> AsyncStream<CLLocation>
}

public struct GetCurrentLocationUseCaseImpl: GetCurrentLocationUseCase {
    
    private let locationRepository: LocationRepository
    
    public init(locationRepository: LocationRepository) {
        self.locationRepository = locationRepository
    }
    
    public func execute() -> AsyncStream<CLLocation> {
        locationRepository.readCurrentLocation(withAccuracy: kCLLocationAccuracyThreeKilometers)
    }
}
