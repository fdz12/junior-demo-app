import UserNotifications

// sourcery: AutoMockable
public protocol RegisterForPushNotificationsUseCase {
    func execute(options: UNAuthorizationOptions, completionHandler: @escaping (Bool, Error?) -> Void)
}

public struct RegisterForPushNotificationsUseCaseImpl: RegisterForPushNotificationsUseCase {
    
    private let pushNotificationsRepository: PushNotificationsRepository
    
    public init(pushNotificationsRepository: PushNotificationsRepository) {
        self.pushNotificationsRepository = pushNotificationsRepository
    }

    public func execute(options: UNAuthorizationOptions, completionHandler: @escaping (Bool, Error?) -> Void) {
        pushNotificationsRepository.register(
            options: options,
            completionHandler: completionHandler
        )
    }
}
