// sourcery: AutoMockable
public protocol RemoteConfigRepository {
    func read(_ key: RemoteConfigCoding) async throws -> Bool
}
