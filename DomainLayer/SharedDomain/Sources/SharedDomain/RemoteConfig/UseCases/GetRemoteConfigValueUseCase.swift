// sourcery: AutoMockable
public protocol GetRemoteConfigValueUseCase {
    func execute(_ key: RemoteConfigCoding) async throws -> Bool
}

public struct GetRemoteConfigValueUseCaseImpl: GetRemoteConfigValueUseCase {
    
    private let remoteConfigRepository: RemoteConfigRepository
    
    public init(remoteConfigRepository: RemoteConfigRepository) {
        self.remoteConfigRepository = remoteConfigRepository
    }
    
    public func execute(_ key: RemoteConfigCoding) async throws -> Bool {
        try await remoteConfigRepository.read(key)
    }
}
