import Foundation

public struct TransactionItem: Equatable, Hashable {
    public let id: Int
    public let type: String
    public let description: String
    public let date: Date
    public let amount: Double
    
    public init (
        id: Int,
        type: String,
        description: String,
        date: Date,
        amount: Double
    ) {
        self.id = id
        self.type = type
        self.description = description
        self.date = date
        self.amount = amount
    }
}
