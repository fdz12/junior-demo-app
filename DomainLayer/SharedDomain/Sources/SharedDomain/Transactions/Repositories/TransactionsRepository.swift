public protocol TransactionsRepository {
    func list() async throws -> [TransactionItem]
//    func detail(id: Int) async throws -> Transaction
}
