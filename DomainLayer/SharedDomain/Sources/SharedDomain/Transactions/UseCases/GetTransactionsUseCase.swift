public protocol GetTransactionsUseCase {
    func execute() async throws -> [TransactionItem]
}

public struct GetTransactionsUseCaseImpl: GetTransactionsUseCase {
    public func execute() async throws -> [TransactionItem] {
        try await transactionsRepository.list()
    }
    
    private let transactionsRepository: TransactionsRepository
    
    public init(transactionsRepository: TransactionsRepository) {
        self.transactionsRepository = transactionsRepository
    }
}
