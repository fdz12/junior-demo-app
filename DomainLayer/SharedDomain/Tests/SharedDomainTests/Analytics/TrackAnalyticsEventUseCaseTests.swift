import SharedDomain
import XCTest

final class TrackAnalyticsEventUseCaseTests: XCTestCase {
    
    // MARK: Dependencies
    
    private let analyticsRepository = AnalyticsRepositoryMock()
    
    // MARK: Tests

    func testExecute() {
        let useCase = TrackAnalyticsEventUseCaseImpl(analyticsRepository: analyticsRepository)
        
        useCase.execute(LoginEvent.screenAppear.analyticsEvent)
        
        XCTAssert(analyticsRepository.createReceivedInvocations == [LoginEvent.screenAppear.analyticsEvent])
    }
}
