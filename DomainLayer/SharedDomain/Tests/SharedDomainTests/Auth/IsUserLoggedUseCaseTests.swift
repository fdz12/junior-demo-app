import SharedDomain
import XCTest

final class IsUserLoggedUseCaseTests: XCTestCase {
    
    // MARK: Dependencies
    
    private let getProfileIdUseCase = GetProfileIdUseCaseMock()
    
    // MARK: Tests
    
    func testExecute() {
        let useCase = IsUserLoggedUseCaseImpl(getProfileIdUseCase: getProfileIdUseCase)
        getProfileIdUseCase.executeReturnValue = AuthToken.stub.userId
        
        let isLogged = useCase.execute()
        
        XCTAssertEqual(isLogged, true)
        XCTAssertEqual(getProfileIdUseCase.executeCallsCount, 1)
    }
}
