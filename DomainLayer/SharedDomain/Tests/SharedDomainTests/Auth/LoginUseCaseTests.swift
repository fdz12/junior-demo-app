import SharedDomain
import XCTest

final class LoginUseCaseTests: XCTestCase {
    
    // MARK: Dependencies
    
    private let authRepository = AuthRepositoryMock()
    private let validateEmailUseCase = ValidateEmailUseCaseMock()
    private let validatePasswordUseCase = ValidatePasswordUseCaseMock()
    
    // MARK: Tests
    
    func testExecute() async throws {
        let useCase = LoginUseCaseImpl(
            authRepository: authRepository,
            validateEmailUseCase: validateEmailUseCase,
            validatePasswordUseCase: validatePasswordUseCase
        )
        
        try await useCase.execute(.stubValid)

        XCTAssert(validateEmailUseCase.executeReceivedInvocations == [LoginData.stubValid.email])
        XCTAssert(validatePasswordUseCase.executeReceivedInvocations == [LoginData.stubValid.password])
        XCTAssert(authRepository.loginReceivedInvocations == [.stubValid])
    }
}
