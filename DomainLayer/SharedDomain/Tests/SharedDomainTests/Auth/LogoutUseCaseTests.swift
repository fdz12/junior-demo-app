import SharedDomain
import XCTest

final class LogoutUseCaseTests: XCTestCase {
    
    // MARK: Dependencies
    
    private let authRepository = AuthRepositoryMock()
    
    // MARK: Tests

    func testExecute() throws {
        let useCase = LogoutUseCaseImpl(authRepository: authRepository)
        
        try useCase.execute()
        
        XCTAssertEqual(authRepository.logoutCallsCount, 1)
    }
}
