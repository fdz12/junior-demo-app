import SharedDomain
import XCTest

final class HandlePushNotificationUseCaseTests: XCTestCase {
    
    // MARK: Dependencies
    
    private let pushNotificationsRepository = PushNotificationsRepositoryMock()
    
    // MARK: Tests

    func testExecute() throws {
        let useCase = HandlePushNotificationUseCaseImpl(pushNotificationsRepository: pushNotificationsRepository)
        pushNotificationsRepository.decodeReturnValue = PushNotification.stub
        
        let pushNotification = try useCase.execute([:])
        
        XCTAssertEqual(pushNotification, PushNotification.stub)
        XCTAssertEqual(pushNotificationsRepository.decodeCallsCount, 1)
    }
}
