import SharedDomain
import XCTest

final class RegisterForPushNotificationsUseCaseTests: XCTestCase {
    
    // MARK: Dependencies
    
    private let pushNotificationsRepository = PushNotificationsRepositoryMock()
    
    // MARK: Tests

    func testExecute() {
        let useCase = RegisterForPushNotificationsUseCaseImpl(pushNotificationsRepository: pushNotificationsRepository)
        
        useCase.execute(options: [.alert, .badge, .sound], completionHandler: { _, _ in })
        
        XCTAssertEqual(pushNotificationsRepository.registerOptionsCompletionHandlerCallsCount, 1)
    }
}
