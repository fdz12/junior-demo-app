import SharedDomain
import XCTest

final class GetRemoteConfigValueUseCaseTests: XCTestCase {
    
    // MARK: Dependencies
    
    private let remoteConfigRepository = RemoteConfigRepositoryMock()
    
    // MARK: Tests

    func testExecute() async throws {
        let useCase = GetRemoteConfigValueUseCaseImpl(remoteConfigRepository: remoteConfigRepository)
        remoteConfigRepository.readReturnValue = true
        
        let value = try await useCase.execute(.profileLabelIsVisible)
        
        XCTAssertEqual(value, true)
        XCTAssert(remoteConfigRepository.readReceivedInvocations == [.profileLabelIsVisible])
    }
}
