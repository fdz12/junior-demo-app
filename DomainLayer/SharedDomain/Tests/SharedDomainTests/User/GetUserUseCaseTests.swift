import SharedDomain
import XCTest

final class GetUserUseCaseTests: XCTestCase {
    
    // MARK: Dependencies
    
    private let userRepository = UserRepositoryMock()
    
    // MARK: Tests

    func testExecute() async throws {
        let useCase = GetUserUseCaseImpl(userRepository: userRepository)
        userRepository.readIdReturnValue = User.stub
        
        let user = try await useCase.execute(.local, id: User.stub.id)
        
        XCTAssertEqual(user, User.stub)
        XCTAssert(userRepository.readIdReceivedInvocations == [(.local, User.stub.id)])
    }
}
