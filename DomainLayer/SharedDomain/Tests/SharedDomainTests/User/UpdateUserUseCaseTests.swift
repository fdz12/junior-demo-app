import SharedDomain
import XCTest

final class UpdateUserUseCaseTests: XCTestCase {
    
    // MARK: Dependencies
    
    private let userRepository = UserRepositoryMock()
    
    // MARK: Tests

    func testExecute() async throws {
        let updatedUser = User(copy: User.stub, bio: "Updated user")
        let useCase = UpdateUserUseCaseImpl(userRepository: userRepository)
        userRepository.updateUserReturnValue = updatedUser
        
        try await useCase.execute(.local, user: updatedUser)
        
        XCTAssert(userRepository.updateUserReceivedInvocations == [(.local, updatedUser)])
    }
}
