import SharedDomain
import XCTest

final class ValidateEmailUseCaseTests: XCTestCase {
    
    // MARK: Tests
    
    func testExecuteEmpty() throws {
        let useCase = ValidateEmailUseCaseImpl()
        
        do {
            try useCase.execute("")
            
            XCTFail("Should throw")
        } catch {
            XCTAssertEqual(error as? ValidationError, .email(.isEmpty))
        }
    }
    
    func testExecuteValid() throws {
        let useCase = ValidateEmailUseCaseImpl()
        
        do {
            try useCase.execute("email@email.com")
            
        } catch {
            XCTFail("Shouldn't throw")
        }
    }
}
