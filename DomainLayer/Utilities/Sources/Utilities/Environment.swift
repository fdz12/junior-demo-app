import Foundation

public enum EnvironmentType {
    case alpha
    case beta
    case production
}

public enum EnvironmentFlavor {
    case debug
    case release
}

public struct Environment {
    public static var type: EnvironmentType = .alpha
    public static var flavor: EnvironmentFlavor = .debug
    public static var locale: Locale = Locale.current
}
