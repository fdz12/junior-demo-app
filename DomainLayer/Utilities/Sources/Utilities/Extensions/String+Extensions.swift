import Foundation

extension String {
    /// Conversion from String to Date using a given formatter.
    func toDate(formatter: DateFormatter = Formatter.Date.default) -> Date? {
        formatter.date(from: self)
    }
}
