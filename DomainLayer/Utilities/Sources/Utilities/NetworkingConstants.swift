public struct NetworkingConstants {
    public static var baseTransactionsURL: String {
        switch Environment.type {
        case .alpha: return "https://stub.bbeight.synetech.cz"
        case .beta: return "https://stub.bbeight.synetech.cz"
        case .production: return "https://stub.bbeight.synetech.cz"
        }
    }
}
