//
//  SwiftUIView.swift
//
//
//  Created by Denis Žuffa on 20/07/2022.
//

import SharedDomain
import SwiftUI
import UIToolkit

struct TransactionsDetailView: View {
    
    @ObservedObject private var viewModel: TransactionsDetailViewModel
    
    init(viewModel: TransactionsDetailViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        ZStack(alignment: .top) {
            Image(uiImage: UIImage(asset: Asset.Images.rectangle)!)
                .resizable()
                .edgesIgnoringSafeArea(.all)
            VStack {
                HStack {
                   Spacer()
                    Image(systemName: "star").foregroundColor(Color.white)
                       .padding(.trailing, 15)
                }
                switch viewModel.state.transaction?.type ?? "" {
                case "house":
                    Image(uiImage: UIImage(asset: Asset.Images.homeIcon3x)!)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 100, height: 100)
                case "kids":
                    Image(uiImage: UIImage(asset: Asset.Images.kidsIcon3x)!)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 100, height: 100)
                case "gas":
                    Image(uiImage: UIImage(asset: Asset.Images.gasIcon3x)!)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 100, height: 100)
                default:
                    Image(uiImage: UIImage(asset: Asset.Images.homeIcon3x)!)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 100, height: 100)
                }
                VStack(alignment: .leading) {
                    Divider().overlay(.gray).padding(.top, 10)
                    VStack(alignment: .leading) {
                        Text(viewModel.state.transaction?.description ?? "")
                            .foregroundColor(Color.white)
                        Text(viewModel.state.transaction?.type ?? "")
                            .font(.footnote)
                            .foregroundColor(Color.gray)
                    }.padding(.leading, 15)
                    Divider().overlay(.gray)
                }
                VStack {
                    VStack {
                        HStack {
                            Text("Amount")
                                .foregroundColor(Color.white)
                            Spacer()
                            Text("\(String(format: "%.2f", viewModel.state.transaction?.amount ?? 0.0)) €")
                                .foregroundColor(Color.white)
                        }
                        Divider().overlay(.gray)
                        HStack {
                            Text("Date")
                                .foregroundColor(Color.white)
                            Spacer()
                            Text(viewModel.state.transaction?.date ?? Date(), style: .date)
                                .foregroundColor(Color.white)
                        }
                    }.padding(.horizontal, 20)
                    Divider().overlay(.gray)
                }.padding(.top, 10)
            }.padding(.top, 10)
        }
        .lifecycle(viewModel)
    }
}

struct TransactionsDetailView_Previews: PreviewProvider {
    static var previews: some View {
        let vm = TransactionsDetailViewModel(
            transaction: TransactionItem(
                id: 0,
                type: "",
                description: "",
                date: Date(),
                amount: 0.0
            ), flowController: nil)
        TransactionsDetailView(viewModel: vm)
    }
}
