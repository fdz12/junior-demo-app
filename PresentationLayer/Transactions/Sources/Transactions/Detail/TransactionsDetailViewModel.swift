import Resolver
import SharedDomain
import SwiftUI
import UIToolkit

final class TransactionsDetailViewModel: BaseViewModel, ViewModel, ObservableObject {

    private var transaction: TransactionItem
    private weak var flowController: FlowController?
    
    init(transaction: TransactionItem, flowController: FlowController?) {
        self.transaction = transaction
        self.flowController = flowController
        super.init()
    }
    
    override func onAppear() {
        super.onAppear()
        executeTask(Task { loadTransactionDetail() })
    }
    
    @Published private(set) var state: State = State()
    
    struct State {
        var transaction: TransactionItem?
    }
    
    enum Intent {
    }
    
    func onIntent(_ intent: Intent) {
    }
    
    private func loadTransactionDetail() {
        state.transaction = self.transaction
    }
}
