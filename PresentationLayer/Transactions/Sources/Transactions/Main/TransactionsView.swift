//
//  SwiftUIView.swift
//
//
//  Created by Denis Žuffa on 20/07/2022.
//

import SwiftUI
import UIToolkit

struct TransactionsView: View {
    
    @ObservedObject private var viewModel: TransactionsViewModel
    
    init(viewModel: TransactionsViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        ZStack(alignment: .top) {
            Image(uiImage: UIImage(asset: Asset.Images.rectangle)!)
                .resizable()
                .edgesIgnoringSafeArea(.all)
            if viewModel.state.transactions.isEmpty {
                Text("Loading...").padding(.trailing, 4.0)
                    .foregroundColor(Color.white)
            }
            ScrollView {
                VStack {
                    ForEach(viewModel.state.transactions, id: \.self) { transaction in
                        VStack {
                            HStack {
                                switch transaction?.type ?? "" {
                                case "house":
                                    Image(uiImage: UIImage(asset: Asset.Images.homeIcon)!)
                                case "kids":
                                    Image(uiImage: UIImage(asset: Asset.Images.kidsIcon)!)
                                case "gas":
                                    Image(uiImage: UIImage(asset: Asset.Images.gasIcon)!)
                                default:
                                    Image(uiImage: UIImage(asset: Asset.Images.bankIcon)!)
                                }
                                VStack(alignment: .leading) {
                                    Text(transaction?.description ?? "")
                                        .foregroundColor(Color.white)
                                    Text(transaction?.type ?? "")
                                        .font(.footnote)
                                        .foregroundColor(Color.gray)
                                }
                                Spacer()
                                Text("\(String(format: "%.2f", transaction?.amount ?? 0.0)) €").padding(.trailing, 4.0)
                                    .foregroundColor(Color.white)
                                Image(systemName: "chevron.right")
                                    .foregroundColor(Color.white)
                            }.padding(.horizontal, 20)
                            Divider()
                                .overlay(.white)
                        }
                        .padding(.bottom, 10)
                        .contentShape(Rectangle())
                        .onTapGesture {
                            viewModel.onIntent(
                                .openTransactionDetail(transaction: transaction!)
                            )
                        }
                    }
                }
            }
        }
        .lifecycle(viewModel)
    }
}

struct TransactionsView_Previews: PreviewProvider {
    static var previews: some View {
        let vm = TransactionsViewModel(flowController: nil)
        TransactionsView(viewModel: vm)
    }
}
