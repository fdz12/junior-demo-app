//
//  File.swift
//
//
//  Created by Denis Žuffa on 20/07/2022.
//

import Resolver
import SharedDomain
import SwiftUI
import UIToolkit

final class TransactionsViewModel: BaseViewModel, ViewModel, ObservableObject {
    
    private weak var flowController: FlowController?
    
    @Injected private(set) var getTransactionsUseCase: GetTransactionsUseCase
    
    init(flowController: FlowController?) {
        self.flowController = flowController
        super.init()
    }
    
    override func onAppear() {
        super.onAppear()
        executeTask(Task { await loadTransactions() })
    }
    
    @Published private(set) var state: State = State()
    
    struct State {
        var transactions: [TransactionItem?] = []
    }
    
    enum Intent {
        case openTransactionDetail(transaction: TransactionItem)
    }
    
    func onIntent(_ intent: Intent) {
        executeTask(Task {
            switch intent {
            case let .openTransactionDetail(transaction):
                openTransactionDetail(transaction)
            }
        })
    }
    
    private func loadTransactions() async {
        do {
            state.transactions = try await getTransactionsUseCase.execute()
        } catch {}
    }
    
    private func openTransactionDetail(_ transaction: TransactionItem) {
        flowController?.handleFlow(TransactionsFlow.transactions(.showDetail(transaction)))
    }
}
