//
//  TransactionsFlowController.swift
//
//
//  Created by Denis Žuffa on 20/07/2022.
//

import SharedDomain
import SwiftUI
import UIKit
import UIToolkit

enum TransactionsFlow: Flow, Equatable {
    case transactions(Transactions)
    
    enum Transactions: Equatable {
        case showDetail(_ transaction: TransactionItem)
    }
}

public final class TransactionsFlowController: FlowController {
    
    override public func setup() -> UIViewController {
        let vm = TransactionsViewModel(flowController: self)
        return BaseHostingController(rootView: TransactionsView(viewModel: vm))
    }
    
    override public func handleFlow(_ flow: Flow) {
        guard let transactionsFlow = flow as? TransactionsFlow else { return }
        switch transactionsFlow {
        case .transactions(let transactionsFlow):
            handleTransactionsFlow(transactionsFlow)
        }
    }
}

extension TransactionsFlowController {
    func handleTransactionsFlow(_ flow: TransactionsFlow.Transactions) {
        switch flow {
        case let .showDetail(transaction): showDetail(transaction)
        }
    }
    
    private func showDetail(_ transaction: TransactionItem) {
        let vm = TransactionsDetailViewModel(transaction: transaction, flowController: self)
        let vc = BaseHostingController(rootView: TransactionsDetailView(viewModel: vm))
        navigationController.show(vc, sender: nil)
    }
}
