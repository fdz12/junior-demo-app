@testable import Transactions
import XCTest

final class TransactionsTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(Transactions().text, "Hello, World!")
    }
}
