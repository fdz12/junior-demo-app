import SwiftUI

public struct SecondaryButtonStyle: ButtonStyle {
    
    public init() {}
    
    public func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .font(AppTheme.Fonts.secondaryButton)
            .foregroundColor(AppTheme.Colors.secondaryButtonTitle)
            .padding()
    }
}

#if DEBUG
struct SecondaryButtonStyle_Previews: PreviewProvider {
    static var previews: some View {
        Button("Lorem Ipsum") {}
            .buttonStyle(SecondaryButtonStyle())
    }
}
#endif
