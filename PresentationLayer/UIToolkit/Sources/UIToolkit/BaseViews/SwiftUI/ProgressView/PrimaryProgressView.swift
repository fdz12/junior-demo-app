import SwiftUI

public struct PrimaryProgressView: View {
    
    public init() {}
    
    public var body: some View {
        ProgressView()
            .progressViewStyle(CircularProgressViewStyle(tint: AppTheme.Colors.progressView))
            .scaleEffect(2)
    }
}

#if DEBUG
struct PrimaryProgressView_Previews: PreviewProvider {
    static var previews: some View {
        PrimaryProgressView()
    }
}
#endif
