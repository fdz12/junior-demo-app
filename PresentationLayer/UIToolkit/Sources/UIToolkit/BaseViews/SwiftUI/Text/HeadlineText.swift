import SwiftUI

public struct HeadlineText: View {
    
    private let content: String
    
    public init(_ content: String) {
        self.content = content
    }
    
    public var body: some View {
        Text(content)
            .font(AppTheme.Fonts.headlineText)
            .foregroundColor(AppTheme.Colors.headlineText)
    }
}

#if DEBUG
struct HeadlineText_Previews: PreviewProvider {
    static var previews: some View {
        HeadlineText("Lorem Ipsum")
    }
}
#endif
