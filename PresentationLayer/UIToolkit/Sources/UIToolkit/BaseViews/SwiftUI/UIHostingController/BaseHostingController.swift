import OSLog
import SwiftUI
import Utilities

public class BaseHostingController<Content>: UIHostingController<Content> where Content: View {
    
    override public init(rootView: Content) {
        super.init(rootView: rootView)
        Logger.lifecycle.info("\(type(of: self)) initialized")
        setupUI()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        Logger.lifecycle.info("\(type(of: self)) initialized")
        setupUI()
    }
    
    deinit {
        Logger.lifecycle.info("\(type(of: self)) deinitialized")
    }
    
    private func setupUI() {
        // Setup background color and back button title
        view.backgroundColor = UIColor(AppTheme.Colors.background)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
    }
}
