import SwiftUI

struct Whisper: View {
    
    private let data: WhisperData
    
    init(_ data: WhisperData) {
        self.data = data
    }
    
    var body: some View {
        GeometryReader { geometry in
            VStack {
                Text(data.message)
                    .font(AppTheme.Fonts.whisperMessage)
                    .foregroundColor(AppTheme.Colors.whisperMessage)
                    .padding(.bottom, 5)
            }
            .frame(
                maxWidth: .infinity,
                maxHeight: geometry.safeAreaInsets.top + 25,
                alignment: .bottom
            )
            .background(data.style.color)
            .ignoresSafeArea()
        }
    }
}

#if DEBUG
struct Whisper_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            Whisper(WhisperData("Lorem Ipsum"))
            Whisper(WhisperData(error: "Error Ipsum"))
        }
    }
}
#endif
