import UIKit

public final class BaseNavigationController: UINavigationController {
    
//    private var statusBarStyle: UIStatusBarStyle = .default
    
    public convenience init() {
        self.init(nibName: nil, bundle: nil)
//        self.statusBarStyle = statusBarStyle
    }
    
//    override public var preferredStatusBarStyle: UIStatusBarStyle {
//        return statusBarStyle
//    }
}
