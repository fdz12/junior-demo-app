import CoreLocation

public extension CLLocationCoordinate2D {

    /// Conversion from CLLocationCoordinate2D to String.
    func toString(withPlaces places: Int = 4) -> String {
        return "\(latitude.rounded(toPlaces: places)); \(longitude.rounded(toPlaces: places))"
    }
}
