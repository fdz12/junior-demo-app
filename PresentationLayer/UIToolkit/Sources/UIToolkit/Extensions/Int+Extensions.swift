import Foundation
import Utilities

extension Int {
    
    /// Conversion from Int to String using a given formatter.
    func toString(formatter: NumberFormatter = Formatter.Number.default) -> String {
        formatter.string(from: NSNumber(value: self)) ?? ""
    }
}
