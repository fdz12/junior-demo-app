import Foundation

extension NSObject {

    /// Class name literal
    class var nameOfClass: String {
        guard let className = NSStringFromClass(self).components(separatedBy: ".").last else { return "N/A" }
        return className
    }
}
