import Foundation

public enum AlertAction: Equatable {
    case showWhisper(_ whisper: WhisperData)
    case hideWhisper
    case showAlert(_ alert: AlertData)

    public static func == (lhs: AlertAction, rhs: AlertAction) -> Bool {
        switch (lhs, rhs) {
        case let (.showWhisper(lhsWhisper), .showWhisper(rhsWhisper)):
            return lhsWhisper == rhsWhisper
        case (.hideWhisper, .hideWhisper):
            return true
        case let (.showAlert(lhsAlert), .showAlert(rhsAlert)):
            return lhsAlert == rhsAlert
        default:
            return false
        }
    }
}
