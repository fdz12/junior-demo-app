@MainActor
public protocol FlowHandler {
    associatedtype Flow
    func handleFlow(_ flow: Flow)
}
